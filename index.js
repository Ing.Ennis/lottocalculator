const days = [
  "sunday",
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
];
/*
    Customize days  Lotto draw 
    Ex: ["monday", "thursday", "friday"]
*/
const daysForLottoDraw = ["sunday"];
const button = document.getElementById("search_draw");
const now = new Date();
const dateDraw = document.getElementById("datatime_draw");
const nextDateDrawTr = document.querySelector("#current_draw");
const rows = document.querySelector("tbody").children;
const table = document.getElementById("draw-lotto-body");
/*
    Set Date now
*/
window.onload = function () {
  dateDraw.value = now.toISOString().slice(0, 16);
};
/*
    Disable button if the input is empty
*/
dateDraw.addEventListener("change", function () {
  if (dateDraw.value == "") {
    dateDraw.style.border = "1px solid #FA6200";
    button.disabled = true;
    button.style.background = "#4a4a4a";
    button.style.border = "#4a4a4a";
    button.classList.remove("hover");
    clearTable();
  } else {
    button.disabled = false;
    button.style.background = "#FA6200";
    button.style.border = "#FA6200";
    button.classList.add("hover");
    dateDraw.style.border = "";
  }
});
button.addEventListener("click", () => {
  clearTable();
  let lottoDraws = getNextLottoDraw();
  for (const row in rows) {
    if (
      typeof lottoDraws[row] != "undefined" &&
      typeof rows[row].children != "undefined"
    ) {
      let text = row < 2 ? "Past" : "Future";
      let columns = rows[row].children;
      let spanDateLotto = document.createElement("span");
      let spanTypeLotto = document.createElement("span");
      spanDateLotto.innerHTML = formatDate(lottoDraws[row]);
      spanTypeLotto.innerHTML = text;
      columns[0].appendChild(spanDateLotto);
      columns[1].appendChild(spanTypeLotto);
    }
  }
});
/*
    Return the dates lotto draw ordered by desc
*/
function getNextLottoDraw() {
  let datesLottoDraw = [];
  let valueDateTime = new Date(dateDraw.value);
  let lottoDraw = getLottoDraw();
  for (let index = 0; index < 5; index++) {
    if (index < 2) {
      if (
        daysForLottoDraw.includes(days[valueDateTime.getDay()]) &&
        !datesLottoDraw.includes(valueDateTime) &&
        valueDateTime.getHours() > 19
      ) {
        valueDateTime = valueDateTime.setDate(valueDateTime.getDate() + 1);
      }
      valueDateTime = lottoDraw.prev(valueDateTime);
    } else {
      let date = index == 2 ? new Date(dateDraw.value) : valueDateTime;
      if (
        daysForLottoDraw.includes(days[date.getDay()]) &&
        !datesLottoDraw.includes(date) &&
        date.getHours() < 20
      ) {
        date = date.setDate(date.getDate() - 1);
      }
      valueDateTime = lottoDraw.next(date);
    }
    datesLottoDraw.push(valueDateTime);
  }
  return datesLottoDraw.slice().sort((a, b) => a - b);
}
/*
    return 2 function
         prev : by date the closer prev date 
         next : by date the closer next date 
*/
function getLottoDraw() {
  return {
    prev: (value) => {
      let copyData = new Date(value);
      let diff = getSortByCantDayDiff(copyData, false);
      copyData.setDate(copyData.getDate() - diff);
      return copyData;
    },
    next: (value) => {
      let copyData = new Date(value);
      let diff = getSortByCantDayDiff(copyData);
      copyData.setDate(copyData.getDate() + diff);
      return copyData;
    },
  };
}
/*
    by customized array Days for Lotto find the  closer date
    return : The number of days difference
*/
function getSortByCantDayDiff(data, isFuture = true) {
  let copyData = data;

  let cantDaysDiff = [];
  let diff = 0;
  for (const dayIndex of daysForLottoDraw) {
    let index = days.findIndex((day) => day === dayIndex);
    let currentDay = copyData.getDay();
    if (!isFuture) {
      diff = currentDay - index == 0 ? 7 : currentDay - index;
      diff = diff < 0 ? currentDay + (7 - index) : diff;
    } else {
      diff = index - currentDay == 0 ? 7 : index - currentDay;

      diff = diff < 0 ? 7 - currentDay + index : diff;
    }
    cantDaysDiff.push(diff);
  }
  return cantDaysDiff.sort()[0];
}
function formatDate(date) {
  return (
    pad2(date.getDate()) +
    "-" +
    pad2(date.getMonth() + 1) +
    "-" +
    date.getFullYear()
  );
}
function pad2(n) {
  return (n < 10 ? "0" : "") + n;
}
function clearTable() {
  for (const row in rows) {
    if (typeof rows[row].children != "undefined") {
      rows[row].setAttribute("animation-active", 0);
      let columns = rows[row].children;
      if (columns.length > 0) {
        columns[0].innerHTML = "";
        columns[1].innerHTML = "";
      }
    }
  }
}
